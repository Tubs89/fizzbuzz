﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FizzBuzzConsole
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var combination = new Tuple<int, string>[]
            {
                new Tuple<int, string>(3, "Fizz"), 
                new Tuple<int, string>(5, "Buzz"), 
            };
            for (var i = 0; i < 50; i++)
            {
                if (i % combination[0].Item1 == 0 && i % combination[1].Item1 == 0) Console.WriteLine(combination[0].Item2 + combination[1].Item2);
                else if (i % combination[0].Item1 == 0) Console.WriteLine(combination[0].Item2);
                else if (i % combination[1].Item1 == 0) Console.WriteLine(combination[1].Item2);
                else Console.WriteLine(i);
            }                
        }

        public object FizzBuzz(int p)
        {
            if (p % 5 == 0 && p % 3 == 0)
            {
                return "FizzBuzz";
            }
            else if (p % 3 == 0)
            {
                return "Fizz";
            }
            else if (p % 5 == 0)
            {
                return "Buzz";
            }
            else
            {
                return p;
            }
        }
    }
}
