﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzzConsole;

namespace FizzBuzzTests
{
    [TestClass]
    public class FizzBuzzUnitTest : Program
    {
        [TestMethod]
        public void Given1Expect1()
        {
            var expected = 1;
            var actual = FizzBuzz(1);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Given3ExpectFizz()
        {
            var expected = "Fizz";
            var actual = FizzBuzz(3);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Given5ExpectBuzz()
        {
            var expected = "Buzz";
            var actual = FizzBuzz(5);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Given15ExpectFizzBuzz()
        {
            var expected = "FizzBuzz";
            var actual = FizzBuzz(15);
            Assert.AreEqual(expected, actual);
        }

    }

}
